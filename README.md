# Prova LEKTO


#Bibliotecas instaladas
- pip install -U robotframework-requests
- pip install -U robotframework-collections
- pip install -U robotframework-jsonlibrary



Comando para rodar as automações 
-     robot desafioapi.robot
-     robot desafiovariables.robot  
-     robot desafioqa.robot  



** DESAFIO 1**
- Implementar scripts de teste automatizados: realizar a automação de testes end-to-end do cenário proposto.
- Cenário: cadastro de agendamento de apresentação com o preenchimento de todos os campos.
- Resultado esperado: os dados devem ser informados adequadamente nos campos relacionados e devem ser e salvos com sucesso.
- Ambiente de Teste: www.lekto.com.br na opção “Agende uma apresentação”.
- Massa de dados: gerar dados aleatórios para realizar o cadastro (é permitido o uso de bibliotecas).
    _Observações:_
    -  Utilizar a ferramenta Robot Framework
    -  O repositório deve ser criado do zero (utilizar gitlab ou github)
    -  Não deve ser utilizado BDD
    -  Informações relevantes devem ser descritas no arquivo readme.md

** DESAFIO 2**

- Implementar scripts automatizados de APIs Rest: realizar a automação de testes da API mencionada.
- Ambiente de Teste: https://api.publicapis.org/entries
- Cenário e orientações: Leia o response code, encontre todos os objetos com propriedade
- “Category: Authentication & Authorization”. Compare, conte e verifique a quantidade
- de objects onde a propriedade acima aparece. Imprima os objetos encontrados noconsole.
- Bônus: Crie o arquivo “.gitlab-ci.yml” no projeto e certifique-se de que o pipeline de CI/CD funcione adequadamente.
    _Observações:_
    -  Utilizar a ferramenta Postman
    -  Pode ser utilizado o mesmo repositório do desafio 1 (utilizar gitlab ou github)
    -  Informações relevantes devem ser descritas no arquivo readme.md




