*** Settings ***
Library    RequestsLibrary
Library    Collections

*** Test Cases ***
Comparar e Contar Objetos com Propriedade Category
    Create Session     api    https://api.publicapis.org
    ${response}    Get    https://api.publicapis.org/entries

    ${json_data}    Evaluate    json.loads($response.content)
    
    ${entries_with_category}    Create List
    FOR    ${entry}    IN    @{json_data['entries']}
        Run Keyword If    '${entry["Category"]}' == 'Authentication & Authorization'    Append To List    ${entries_with_category}    ${entry}
    END

    ${num_entries}    Get Length    ${entries_with_category}
    Log    ${num_entries} objetos encontrados com a propriedade "Category" igual a "Authentication & Authorization"
    Log Many    @{entries_with_category}

    Delete All Sessions