** Settings **
Library  SeleniumLibrary

** Variables **


** Keywords **
Acessar site
    Open Browser    https://www.lekto.com.br    chrome

Aguardar site carregar
    Sleep  5s

Clicar botão Agende uma apresentação
    Click Element                xpath=//a[contains(text(), 'Agende uma apresentação')]
    Sleep  5s

Prencher campos
    Input Text                   id=text-yui_3_17_2_1_1593033605278_167399-field                               Felipe
    Click Element                xpath=//select[@aria-label='Sou']
    Sleep   5s
    Select From List by Label    id=select-yui_3_17_2_1_1593033605278_168358-field                             Educador(a)
    Input Text                   xpath=//input[@id='email-yui_3_17_2_1_1593039425574_26430-field']             felipe@gmail.com
    Input Text                   id=text-yui_3_17_2_1_1593039425574_26431-field                                61999999999
    Input Text                   xpath=//textarea[@id='textarea-yui_3_17_2_1_1593039425574_26432-field']       desafioqa
    Sleep   5s

Clicar botao quero a lekto na minha escola!
    Click Button                 xpath=//span[@class='form-submit-button-label' and text()='Quero a Lekto na minha escola!']/ancestor::button
    Sleep   5s



** Test Cases **
Cenário 1: Agende uma apresentação
    Acessar site
    Aguardar site carregar
    Clicar botão Agende uma apresentação
    Prencher campos
    Clicar botao quero a lekto na minha escola!
    