** Settings **
Library  SeleniumLibrary

** Variables **
${SITE_LEKTO}               https://www.lekto.com.br
${AGENDE_APRESENTACAO}      xpath=//a[contains(text(), 'Agende uma apresentação')]    
${NOME}                     Felipe Araujo
${SOU}                      Educador(a)                       
${EMAIL}                    felipe@gmail.com
${TELEFONE}                 61999999999
${MENSAGEM}                 Desafio QA 
${BUTTON}                   xpath=//span[@class='form-submit-button-label' and text()='Quero a Lekto na minha escola!']/ancestor::button


** Keywords **
Acessar site
    Open Browser    ${SITE_LEKTO}    chrome

Aguardar site carregar
    Sleep  5s

Clicar botão Agende uma apresentação
    Click Element                ${AGENDE_APRESENTACAO}    
    Sleep  5s

Prencher campos
    Input Text                   id=text-yui_3_17_2_1_1593033605278_167399-field                               ${NOME}
    Click Element                xpath=//select[@aria-label='Sou']
    Sleep   5s
    Select From List by Label    id=select-yui_3_17_2_1_1593033605278_168358-field                             ${SOU}
    Input Text                   xpath=//input[@id='email-yui_3_17_2_1_1593039425574_26430-field']             ${EMAIL} 
    Input Text                   id=text-yui_3_17_2_1_1593039425574_26431-field                                ${TELEFONE} 
    Input Text                   xpath=//textarea[@id='textarea-yui_3_17_2_1_1593039425574_26432-field']       ${MENSAGEM} 
    Sleep   5s

Clicar botao quero a lekto na minha escola!
    Click Button                ${BUTTON}                
    Sleep   5s



** Test Cases **
Cenário 1: Agende uma apresentação
    Acessar site
    Aguardar site carregar
    Clicar botão Agende uma apresentação
    Prencher campos
    Clicar botao quero a lekto na minha escola!
    